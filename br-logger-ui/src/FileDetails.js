import React, { Component } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import "react-tabs/style/react-tabs.css";

export class FileDetails  extends Component{

    render() {
        return (
            <Tabs>
                <TabList>
                    <Tab>File Chart</Tab>
                    <Tab>File Data</Tab>
                </TabList>
                <TabPanel>
                    <h1>File Chart</h1>
                </TabPanel>
                <TabPanel>
                    <h1>File Data</h1>
                </TabPanel>
            </Tabs>
        );
    }
}
    
