import React, { Component } from 'react';
import { FilesList } from "./FilesList";
import {FileDetails} from "./FileDetails";
import { Grid } from '@material-ui/core';
import './App.css';

export default class App extends Component {
    render() {
        return (
            <div>
            <div className="App-header">
                <h1 className="App-title">BR Logger</h1>
            </div>
          <Grid container className="App">
            <Grid item>
                  <FilesList className="App-intro" />
            </Grid>
            <Grid item>
                <FileDetails/>
            </Grid>
          </Grid>
            </div>
        );
    }
}