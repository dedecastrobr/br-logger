import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import FolderIcon from '@material-ui/icons/Folder';


export class FilesList extends Component{

    state = {
        files: []
    };

    componentWillMount() {
        console.log('will');
        this.getFilesList()
            .then(res => {
                this.setState({ files: res.files});
            })
            .catch(err => {
                this.setState({ files: [err] })
            });
    }

    getFilesList() {
        return new Promise((resolve, reject) => {
            resolve(fetch('/list-files')
                .then(res => res.json())
                .catch(err => reject(err))
            );
        });
    }

    loadFile(fileName) {
        return new Promise((resolve, reject) => {
            resolve(fetch('/load-file/'+fileName)
                .then(res => res.json())
                .catch(err => reject(err))
            );
        });
    }

    render() {
        console.log(this.state.files);
        this.listItems = this.state.files.map((fileName) =>
            <ListItem value={fileName} key={fileName} divider button onClick={() => this.loadFile(fileName)}>
                    <ListItemAvatar>
                        <Avatar>
                            <FolderIcon />
                        </Avatar>
                    </ListItemAvatar>
                <ListItemText primary={fileName} />
            </ListItem>
        );
        return (
            <div>
                <h3>Files</h3>
                <div>
                    <List>
                        {this.listItems}
                    </List>
                </div>
            </div>
        );
    }

}