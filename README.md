# BR-LOGGER

Web UI to read logs from [BR Charge](www.brcharge.com.br) equipments.

You will need [node](https://nodejs.org/en/) to run this application. Install it first.

Instal instructions:
1. Clone the project;
2. From the root folder run:
```
npm install
```

Run instructions:
1. From `br-logger` folder execute: 
```
node server.js
```
2. From `br-logger-ui`:
```
npm start
```
3. If the browser doesn't show up automatically go to:
```
http://localhost:3000
```




Made with Node.js, React and love by André Castro.

