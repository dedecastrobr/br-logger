const express = require('express');
const app = express();
const port = 3232;

const fs = require('fs'), path = require('path');
const logfolder = path.join(__dirname, 'logs');

app.listen(port, () => console.log(`Listening on port ${port}`));

app.get('/list-files', (req, res) => {
   console.log('GET: /list-files');
   listFiles(logfolder)
       .then((list) => { 
           res.send({ status: 200, files: list });
       })
       .catch((err) => {
            console.error(err)
           res.send({ status: 400,  error: err});
       });
});

app.get('/load-file/:file_name', (req, res) => {
    console.log('GET: /load-file', req.params.file_name);
    getLines(req.params.file_name)
    .then((entries) => {
        res.send({ status: 200, file_entries: entries });
    })
    .catch((err) => {
        console.error(err)
        res.send({ status: 400,  error: err});
    })
});

function getLines(fileName) {
    let entries = [];
    let values = [];
    const regex = /^(\S+)\s+(\S+)\s+\[([^\]]+)\]\s+(.+)\s\-\s(.+)$/gm;
    return new Promise (function(resolve, reject) {
        fs.readFile(logfolder+'/'+fileName, 'utf-8', function(err, data) {
            try {
                data.split('\r\n').map((line) => {
                    values = line.split(regex);
                    entries.push({
                        "date": values[1],
                        "time": values[2],
                        "type": values[3],
                        "device": values[4],
                        "message": values[5] 
                    })
                });
                resolve(entries);
            } catch (error) {
                reject(error);
            }
        });
    });
}

function listFiles(dir) {
    return new Promise(function (resolve, reject) {
        fs.readdir(dir, function(err, files) {
            err ? reject(err) : resolve(files);
        });
    });
}